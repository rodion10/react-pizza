import React from 'react';
import { useSelector } from 'react-redux';

import Categories from '../components/Categories';
import SortPupap from '../components/Sort';
import Pagination from '../components/Pagination';
import { setActiveCategory, setCurrentPage, filterSelector } from '../redux/slices/filterSlice';
import { fetchItems, pizzaSelector } from '../redux/slices/pizzaSlice';
import { PizzaList } from '../components/PizzaList';
import { useAppDispatch } from '../redux/store';


const Home: React.FC = () => {
  const { activeCategory, sort, currentPage, searchValue } = useSelector(filterSelector);
  const { items, status } = useSelector(pizzaSelector);
  const search = searchValue ? `&search=${searchValue}` : '';
  const dispatch = useAppDispatch();

  const setCategory = React.useCallback((id: number) => {
    dispatch(setActiveCategory(id));
  }, [])
  
  const setPage = (page: number) => {
    dispatch(setCurrentPage(page));
  };

  const fetchPizzas = async () => {
    dispatch(
      fetchItems({ sort, activeCategory, search, currentPage }));
  };

  React.useEffect(() => {
    fetchPizzas();
  }, [activeCategory, sort.propertySort, searchValue, currentPage]);



  return (
    <div className='container'>
      <div className='content__top'>
        <Categories activeCategory={activeCategory} setCategory={setCategory} />
        <SortPupap value={sort}/>
      </div>
      <h2 className='content__title'>Все пиццы</h2>
      {status === 'error' ? (
        <div>
          <h1>Ошибка Загрузки</h1>
        </div>
      ) : (
        <>
          <PizzaList items={items} status={status} />
          <Pagination onChangePage={(page: number) => setPage(page)} />
        </>
      )}
    </div>
  );
}

export default Home