import React from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';

export const FullPizza: React.FC = () => {
  const { id } = useParams();
  const navigate = useNavigate()
  const [pizza, setPizza] = React.useState<
    {
      imageUrl: string;
      title: string;
      price: number;
    }
  >();

  React.useEffect(() => {
    async function FetchPizza() {
      try {
        const { data } = await axios.get('https://6363ae498a3337d9a2e450b2.mockapi.io/Items/' + id);
        setPizza(data);
      } catch (error) {
        alert('Ошибка получения пиццы!');
        navigate("/")
      }
    }
    FetchPizza();
  }, []);

  if (!pizza) {
    return <>'Загрузка!'</>;
  }

  return (
    <div className='pizza-block-wrapper'>
      <div className='pizza-block'>
        <img className='pizza-block__image' src={pizza.imageUrl} alt='Pizza' />
        <h4 className='pizza-block__title'>{pizza.title}</h4>
        <div className='pizza-block__price'>{pizza.price} ₽</div>
      </div>
    </div>
  );
};
