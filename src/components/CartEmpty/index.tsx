import React from 'react';
import { Link } from 'react-router-dom';
import emptyCart from '../../assets/img/empty-cart.png';
import styles from './CartEmpty.module.scss';

export const CartEmpty: React.FC = () => {
  return (
    <div className={styles.empty}>
      <h2>Корзина пуста</h2>
      <p>
        Скорее всего, вы не заказали еще пиццу. <br></br>
        Для того, чтобы заказать пиццу, перейдите на главную страницу.
      </p>
      <img src={emptyCart} alt="Пустая корзина" />
      <Link to={'/'} className='button button-black'>
        <span>Вернуться назад</span>
      </Link>
    </div>
  );
};