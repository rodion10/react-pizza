import React from 'react';
import styles from './Categories.module.scss';

type CategoriesProps = {
  activeCategory: number;
  setCategory: (i: number) => void;
}

const categories = ['Все', 'Мясные', 'Вегетарианская', 'Гриль', 'Острые', 'Закрытые'];

const Categories: React.FC<CategoriesProps> = React.memo(({ activeCategory, setCategory }) => {
  return (
    <div className={styles.categories}>
      <ul>
        {categories.map((category, i) => (
          <li
            key={i}
            onClick={() => setCategory(i)}
            className={activeCategory === i ? 'active' : ''}
          >
            {category}
          </li>
        ))}
      </ul>
    </div>
  );
})

export default Categories