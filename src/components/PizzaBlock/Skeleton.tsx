import React from "react"
import ContentLoader from "react-content-loader"

type SkeletonProps = {
  key: number
}

const Skeleton: React.FC<SkeletonProps> = ( props ) => (
  <ContentLoader
    className="pizza-block"
    speed={2}
    width={280}
    height={470}
    viewBox="0 0 280 470"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}
  >


    
    <circle cx="130" cy="130" r="130" />
    <rect x="0" y="279" rx="0" ry="0" width="270" height="30" />
    <rect x="0" y="325" rx="0" ry="0" width="270" height="77" />
    <rect x="120" y="425" rx="30" ry="30" width="150" height="42" />
    <rect x="6" y="425" rx="0" ry="0" width="70" height="40" />
  </ContentLoader>
)

export default Skeleton