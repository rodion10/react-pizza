import React from 'react';
import debounce from 'lodash.debounce';
import { useDispatch, useSelector } from 'react-redux';

import search_icon from '../../assets/img/search_icon.svg';
import deleteSvg from '../../assets/img/delete_icon.svg';
import styles from './Search.module.scss';
import { filterSelector, setSearchValue } from '../../redux/slices/filterSlice';


const Search: React.FC = () => {
  const [value, setValue] = React.useState<string>();
  const inputRef = React.useRef<HTMLInputElement>(null);
  const dispatch = useDispatch();
  const { searchValue } = useSelector(filterSelector);

  const onClickClear = () => {
    dispatch(setSearchValue(""))
    setValue('');
    inputRef.current?.focus();
  };

  const updateSearchValue = React.useCallback(
    debounce((str: string) => {
      dispatch(setSearchValue(str))
    }, 300),
    [],
  );

  const onChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
    updateSearchValue(event.target.value);
  };

  return (
    <div className={styles.root}>
      <img className={styles.search} src={search_icon} alt='Delete svg' />
      <input
        ref={inputRef}
        value={value}
        onChange={(event) => onChangeInput(event)}
        className={styles.input}
        placeholder='y`r not WeaK AF'
      ></input>
      {searchValue && (
        <img
          onClick={() => onClickClear()}
          className={styles.delete}
          src={deleteSvg}
          alt='Delete svg'
        />
      )}
    </div>
  );
}

export default Search