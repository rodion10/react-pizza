import React from 'react';

import Skeleton from '../PizzaBlock/Skeleton';
import PizzaBlock from '../PizzaBlock';
import './style.scss';
import { PizzaItem } from '../../redux/slices/pizzaSlice';

type PizzaList = {
  items: PizzaItem[];
  status: string;
}

export const PizzaList: React.FC<PizzaList> = ({ items, status }) => {
  return (
    <div className='content__items'>
      {status === 'loading'
        ? [...new Array(6)].map((_, i) => <Skeleton key={i} />)
        : items.map((obj: any) => (
            <PizzaBlock {...obj} key={obj.id} />
        ))}
    </div>
  );
};
