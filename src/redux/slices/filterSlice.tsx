import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';

export type Sort = {
  name: string;
  propertySort: "rating" | "price" | 'title';
}

interface FilterSliceState {
  currentPage: number;
  activeCategory: number;
  searchValue: string;
  sort: Sort
}

const initialState: FilterSliceState = {
  currentPage: 1,
  activeCategory: 0,
  searchValue: '',
  sort: {
    name: 'популярности',
    propertySort: 'rating',
  },
};

const filterSlice = createSlice({
  name: 'filters',
  initialState,
  reducers: {
    setSearchValue(state, action: PayloadAction<string>) {
      state.searchValue = action.payload;
    },
    setActiveCategory(state, action: PayloadAction<number>) {
      state.activeCategory = action.payload;
    },
    setSort(state, action: PayloadAction<Sort>) {
      state.sort = action.payload;
    },
    setCurrentPage(state, action: PayloadAction<number>) {
      state.currentPage = action.payload;
    },
  },
});

export const filterSortSelector = (state: RootState) => state.filter.sort;
export const filterSelector = (state: RootState) => state.filter;

export const { setSort, setActiveCategory, setCurrentPage, setSearchValue } = filterSlice.actions;

export default filterSlice.reducer;
