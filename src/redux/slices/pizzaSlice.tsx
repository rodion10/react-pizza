import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { RootState } from '../store';
import { Sort } from './filterSlice';

export type PizzaItem = {
  id: string;
  title: string;
  price: number;
  imageUrl: string;
  sizes: number[];
  types: number[];
}
export enum Status {
  LOADING = 'loading',
  SUCCESS = 'success',
  ERROR = 'error'
}

interface PizzaSliceState {
  items: PizzaItem[],
  status: Status,
}

const initialState: PizzaSliceState = {
  items: [],
  status: Status.LOADING
};

type FetchItems = {
  activeCategory: number;
  currentPage: number;
  search: string
  sort: Sort
}

export const fetchItems = createAsyncThunk<PizzaItem[], FetchItems>('pizza/fetchItems', async (props) => {
  const { data } = await axios.get<PizzaItem[]>(
    `https://6363ae498a3337d9a2e450b2.mockapi.io/Items?&page=${props.currentPage}&limit=4${props.search
    }&${props.activeCategory > 0 ? `categori${props.activeCategory}` : ''}&sortBy=${props.sort.propertySort
    }&order=desc`,
  );
  return data;
});

const pizzaSlice = createSlice({
  name: 'pizza',
  initialState,
  reducers:
  {
    // setItems(state, action) {
    //   state.items = action.payload;
    // },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchItems.pending, (state) => {
        state.status = Status.LOADING;
        state.items = [];
      })
      .addCase(fetchItems.fulfilled, (state, action) => {
        state.items.push(...action.payload);
        state.status = Status.SUCCESS;
      })
      .addCase(fetchItems.rejected, (state) => {
        state.status = Status.ERROR;
        state.items = [];
      });
  },
});

export const pizzaSelector = (state: RootState) => state.pizza


// export const { setItems } = pizzaSlice.actions;

export default pizzaSlice.reducer;