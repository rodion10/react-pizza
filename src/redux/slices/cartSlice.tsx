import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';

export type TCartItem = {
  id: string;
  title: string;
  price: number;
  imageUrl: string;
  size: number;
  type: string;
  count: number;
}

interface CartSliceState {
  price: number;
  items: TCartItem[];
}

const initialState: CartSliceState = {
  price: 0,
  items: [],
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItem(state, action: PayloadAction<TCartItem>) {
      const findItem = state.items.find((item) => item.id === action.payload.id);
      if (findItem) {
        findItem.count++;
      } else {
        state.items.push({
          ...action.payload,
          count: 1,
        });
      }
      state.price = state.items.reduce((sum, obj) => {
        return obj.price * obj.count + sum;
      }, 0);
    },
    decrementItem(state, action: PayloadAction<string>) {
      const findItem = state.items.find((item) => item.id === action.payload); // action.payload.id
      if (findItem) {
        findItem.count--;
      }
      state.price = state.items.reduce((sum, obj) => {
        return obj.price * obj.count + sum;
      }, 0);
    },
    removeItem(state, action: PayloadAction<string>) {
      state.items = state.items.filter((obj) => obj.id !== action.payload);
      state.price = state.items.reduce((sum, obj) => {
        return obj.price * obj.count + sum;
      }, 0);
    },
    clearCart(state) {
      state.price = 0;
      state.items = [];
    },
  },
});

export const cartSelector = (state: RootState) => state.cart;
export const cartItemCountSelector = (state: RootState, id: string) => state.cart.items.find((obj) => obj.id === id);

export const { addItem, removeItem, clearCart, decrementItem } = cartSlice.actions;

export default cartSlice.reducer;
